package com.webage.spring.samples.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellloAPI
{
    public static void main(String[] args) { 
	SpringApplication.run(HellloAPI.class, args); 
}
}