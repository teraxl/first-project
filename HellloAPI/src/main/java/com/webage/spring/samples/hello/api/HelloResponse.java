package com.webage.spring.samples.hello.api;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class HelloResponse 
{
	private int age;
	private String message;
	public HelloResponse(String message, int age)
	{		
		this.message = message;
		this.age = age;
	}
	
	@JsonGetter(value = "message")
	public String getMessage() 
	{
		return message;
	}
	
	@JsonSetter("message")
	public void setMessage(String message) 
	{
		this.message = message;
	}	
	
	@JsonGetter(value = "age")
	public int getAge()
	{
		return age;	
	}
	
	@JsonSetter("age")
	public void setAge(int age) 
	{
		this.age = age;
	}
}